import dsfml.window : Event, VideoMode, Keyboard, ContextSettings;
import dsfml.graphics : RenderWindow, Color, View, Window;
import dsfml.system : Vector2f;

import source.simulation : Simulation;

void main()
{
    string title = "WTF is this?";
    uint height = 200;
    uint width = 200;

    ContextSettings settings = ContextSettings();
    settings.antialiasingLevel = 16;
    RenderWindow window = new RenderWindow(VideoMode(width, height, 32), title, Window.Style.DefaultStyle, settings);
    window.setVerticalSyncEnabled(true);

    //View view = new View();
    //view.center(Vector2f(0, 0));
    //view.size(Vector2f(width, height));
    //window.view = view;

    void cQuit()
    {
        window.close();
    }

    Simulation sim = new Simulation(window);

    sim.setup();

    while (window.isOpen())
    {
        Event event;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
                case event.EventType.Closed:
                    cQuit();
                    break;

                case event.EventType.KeyReleased:
                    switch (event.key.code)
                    {
                        case Keyboard.Key.W:
                            if (event.key.control == true)
                            {
                                cQuit();
                            }
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
        }
        window.clear(Color(255, 255, 255));

        sim.draw();

        window.display();
    }
}
