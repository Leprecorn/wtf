module source.simulation;

import dsfml.graphics : RenderWindow, Texture, Sprite;
import dsfml.system : Vector2f;

class Simulation
{
    private RenderWindow window;

    this(RenderWindow window)
    {
        this.window = window;
    }

    immutable int width = 200;
    immutable int height = 200;
    private ubyte[4*this.width*this.height] pixels;
    Texture textur;
    Sprite sprite;

    float[height][width] current;
    float[height][width] previous;
    float[height][width] temp;
    float damping = 0.9;

    public void setup()
    {
        for (int x = 0; x < this.width; x++)
        {
            for (int y = 0; y < this.height; y++)
            {
                this.pixels[(x + y * this.width) * 4 + 0] = 0;  // R
                this.pixels[(x + y * this.width) * 4 + 1] = 0;  // G
                this.pixels[(x + y * this.width) * 4 + 2] = 0;  // B
                this.pixels[(x + y * this.width) * 4 + 3] = 255;// A
            }
        }

        this.textur = new Texture();
        this.textur.create(width, height);
        this.textur.updateFromPixels(cast(const(ubyte[]))this.pixels, width, height, 0, 0);

        this.sprite = new Sprite();
        this.sprite.setTexture(this.textur);

        this.previous[this.height/2][this.width/2] = 100;
    }

    public void draw()
    {
        for (uint i = 1; i < width -1; i++)
        {
            for (uint j = 1; j < height -1; j++)
            {
                this.current[i][j] = (
                    this.previous[i-1][j] +
                    this.previous[i+1][j] +
                    this.previous[i][j+1] +
                    this.previous[i][j-1]) /2 -
                    this.current[i][j];

                this.current[i][j] = cast(int)(this.current[i][j] * this.damping);
                this.pixels[(i + j * this.width) * 4 + 0] = cast(ubyte)this.current[i][j];
                this.pixels[(i + j * this.width) * 4 + 1] = cast(ubyte)this.current[i][j];
                this.pixels[(i + j * this.width) * 4 + 2] = cast(ubyte)this.current[i][j];
            }
        }

        this.temp = this.previous;
        this.previous = this.current;
        this.current = this.temp;


        this.textur.updateFromPixels(cast(const(ubyte[]))this.pixels, width, height, 0, 0);
        this.window.draw(sprite);
    }
}
